import request from '@/utils/request'

export function fetchList(data) {
  return request({
    url: '/api/wx/text/template/list',
    method: 'post',
    params:data
  })
}

export function createRow(data) {
  return request({
    url: '/api/wx/text/template/add',
    method: 'post',
    data
  })
}

export function updateRow(data) {
  return request({
    url: '/api/wx/text/template/update',
    method: 'post',
    data
  })
}
export function deleteRow(data) {
  return request({
    url: '/api/wx/text/template/delete',
    method: 'post',
    params:data
  })
}

export function fetchAllTextTpl() {
  return request({
    url: '/api/wx/text/template/listAll',
    method: 'post'
  })
}
